// const titleInput = document.getElementById("title");
const titleInput = document.querySelector("input[name='title']");
const statusInput = document.getElementById("status");
let selectedIndex = -1;
const submitButton = document.querySelector('button[type="submit"]');

// const outputDiv = document.getElementById("output");

const outputDiv = document.querySelector(`div[id='output']`);

let todoList = [];

const addItem = (event) => {
  event.preventDefault();
  titleInput.classList.remove("error");

  if (titleInput.value == "") {
    titleInput.classList.add("error");
  } else {
    if (selectedIndex > -1) {
      todoList[selectedIndex] = {
        title: titleInput.value,
        status: statusInput.value,
      };
      selectedIndex = -1;
      submitButton.innerHTML = "Add";
    } else {
      todoList.push({
        title: titleInput.value,
        status: statusInput.value,
      });
    }

    titleInput.value = "";
    statusInput.value = "incomplete";

    displayItems();
  }
};

const deleteItem = (index) => {
  todoList = todoList.filter((v, key) => key !== index);
  displayItems();
};

const editItem = (index) => {
  titleInput.value = todoList[index].title;
  statusInput.value = todoList[index].status;
  selectedIndex = index;

  submitButton.innerHTML = "Save";
};

// Create, Read, Update, Delete

const displayItems = () => {
  outputDiv.innerHTML = "";
  todoList.map((item, index) => {
    outputDiv.innerHTML += `
    <div>${item.title}   
      <input type="checkbox"  ${item.status == "complete" && "checked"}/>  
      <button onclick="deleteItem(${index})">Delete</button>  
      <button onclick="editItem(${index})">Edit</button>  
    </div>`;
  });
};

let x = 10;

// Ternary operator (comparision)
// x>5 ? console.log('sth') : console.log('sthelse')
// x>5 && console.log('x is greater than 5')

// Normal way without using ternary operator
//   if (item.status == "incomplete") {
//     outputDiv.innerHTML += `
//           <div>${item.title}
//             <input type="checkbox" />
//             <button onclick="deleteItem(${index})">Delete</button>
//           </div>
//           `;
//   } else {
//     outputDiv.innerHTML += `
//           <div>
//             ${item.title}
//             <input type="checkbox" checked/>
//             <button onclick="deleteItem(${index})">Delete</button>
//           </div>
//           `;
//   }
// });

// accounts=[{
//   title:'eswea',
//   amount:800
// }]

// transactions=[]

// addItem=()=>{
//   transactions.push(newITem)

//   // accounts[newItem.account]

//   // accounts[esewa]

//   // accounts.esewa

//   newITem={
//     amount:200,
//     type:'income',
//     account:'esewa'
//   }

//   let accountIndex=accounts.indexOf(item=>item.title==newITem.account)

//   accounts[accountIndex]={
//     title:accouts[accountIndex].title,
//     amount: newITem.type=='income'? accouts[accountIndex].amount+newITem.amount:accouts[accountIndex].amount-newITem.amount
//   }

// }
