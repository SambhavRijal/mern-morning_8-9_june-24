const outputDiv = document.querySelector(".output");

const addItem = (event) => {
  event.preventDefault();

  const todoItem = document.querySelector('input[name="title"]').value;
  console.log(todoItem);

  const li = document.createElement("div");

  li.innerHTML = todoItem;

  const deleteButton = document.createElement("button");
  deleteButton.innerHTML = "Delete";
  //   deleteButton.innerHTML = "Done";

  deleteButton.addEventListener("click", () => {
    // outputDiv.removeChild(li);
    li.classList.toggle("checked");
  });

  li.appendChild(deleteButton);

  outputDiv.appendChild(li);
};

const img = document.querySelector(".main-image");
const changeImage = () => {
  img.src =
    "https://cdn.pixabay.com/photo/2017/06/14/07/05/siberian-2401287_1280.jpg";
};

// li=<div></div>

// li=<div>hi</div>

// deleteButton=<button></button>
// deleteButton=<button>Delete</button>

// li=<div>hi    <button>Delete</button> </div>

// <div class="output">
//     <div>Hello    <button>Delete</button> </div>
//     <div>hi    <button>Delete</button> </div>
// </div>
