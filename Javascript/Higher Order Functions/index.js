// map, filter, sort, every, some, indexOf, reduce

let names = ["sam", "bha", "vri", "jal"];
// Mapping
names.map((item) => {
  console.log(item);
});

names.map((item) => console.log(item));

// {
//     title:''
//     status:'incomplete' -> 'complete'
// }

// checkSth=(selectedIndex)=>{
//     todoItems[index].status='complete'

//      todoItems=todoItems.map(item,index=>{
//         if(index==selectedIndex){
//             if(item.status=='complete'){
//                 return {
//                     title:item.title,
//                     status:'incomplete'
//                 }
//             }else{
//                 return {
//                     title:item.title,
//                     status:'complete'
//                 }
//             }
//         }else{
//             return item
//         }
//     })

// }

// let modifiedNames = names.map((item) => item + " Rijal");   --> Way 1

// Way 2
let modifiedNames = names.map((item) => {
  return item + " Rijal";
});

console.log(modifiedNames);

// Filter
let filteredNames = names.filter((item) => {
  return item != "sam";
});
console.log(filteredNames);

console.log(names.indexOf("vri"));
let filteredNamesByIndex = names.filter((v, key) => key != 2);
console.log(filteredNamesByIndex);

let numbers = [3, 9, 1, 7, 3, 7, 5];
// Sort
console.log(numbers.sort());

// Some
// let result = numbers.some((item) => item % 2 == 0);
let result = numbers.some((item) => item < 6);
console.log(result);

// Every
let secondResult = numbers.every((item) => item < 6);
console.log(secondResult);

// Reduce
const array1 = [1, 2, 3, 4];

// 0 + 1 + 2 + 3 + 4
const initialValue = 0;
const sumWithInitial = array1.reduce(
  (accumulator, currentValue) => accumulator + currentValue
);

console.log(sumWithInitial);

// c,a
// 1,0= 1
// 2,1= 3
// 3,3=6
// 4,6=10

// reduce
const pokemon = [
  {
    name: "charmander",
    description: {
      type: "sth",
      sth: "sth",
    },
  },
  { name: "squirtle", type: "water" },
  { name: "bulbasaur", type: "grass" },
  { name: "charmander", power: "flight" },
];

acc = {
  charmander: { name: "charmander", type: "fire" },
  squirtle: { name: "squirtle", type: "water" },
  bulbasaur: { name: "bulbasaur", type: "grass" },
};

const total = pokemon.reduce((acc, item) => {
  acc[item.name] = {
    ...acc[item.name],
    ...item,
  };
  return acc;
}, {});

console.log(total);

// todoItems.map((item,index)=>{
//     '<div>    </div><button onclick="deleteItem(${index})>Delete</button>'
// })

// let x=5
// let y=10

// const add=()=>{
//     let total=x+y
//     return total
// }

// let totalValue=add()
