// for loop
// while
// do while
// foreach

// string, int (float),boolean, array, object

// let pass = false;

let students = ["Sambhav", "Rijal", "Prashant", "Tanisi"];

for (i = 1; i <= 10; i++) {
  console.log(`Multiplication table for ${i} is:`);
  for (j = 1; j <= 10; j++) {
    console.log(`${i} * ${j} = `, i * j);
  }
}

let point = 10;

while (point <= 20) {
  console.log(`Point is ${point}`);
  point++;
}

let secondPoint = 10;

do {
  console.log(`Second Point is ${secondPoint}`);
  secondPoint++;
} while (secondPoint == 3);

console.log(students);

console.log(students[2]);

for (i = 0; i < students.length; i++) {
  console.log("Name is:", students[i]);
}

for (studentName in students) {
  console.log(students[studentName]);
}

console.log("For each loop");
students.forEach((item, index) => {
  console.log(item);
});

let todoItems = [];

let outputDiv = document.querySelector(".todo-items");

todoItems.forEach((item, index) => {
  outputDiv.innerHTML = outputDiv.innerHTML + item;
});

const addItem = () => {
  const input = document.getElementById("item");
  const value = input.value;
  todoItems.push(value);
  console.log(todoItems);
  input.value = "";

  outputDiv.innerHTML = "";

  todoItems.forEach((item, index) => {
    outputDiv.innerHTML = outputDiv.innerHTML + `<div>${item}</div>`;
  });
};
