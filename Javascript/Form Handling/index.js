let inputValues = {
  title: "",
  status: "",
};

let todoItems = [];

function changeInput(event) {
  inputValues = {
    ...inputValues,
    [event.target.name]: event.target.value,
  };
}

function addItem() {
  todoItems.push(inputValues);
  console.log(todoItems);
  resetForm();
}8

document.querySelector("#form").addEventListener("submit", (event) => {
  event.preventDefault();
  addItem();
});

function resetForm() {
  inputValues = {
    title: "",
    status: "",
  };
}
