const person = {
  name: "sambjav",
  age: 16,
  status: "single",
};

let myData = {
  principle: 0,
  time: 0,
  rate: 0,
};

console.log(person.age);

const addItem = (event) => {
  event.preventDefault();
  console.log(myData);
};

const changeData = (event) => {
  console.log(event.target);

  myData = {
    ...myData,
    [event.target.name]: event.target.value,
  };
};
