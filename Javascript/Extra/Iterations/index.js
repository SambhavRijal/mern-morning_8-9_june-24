const inputDiv = document.querySelector('input[name="title"]');
const outputDiv = document.querySelector(".output");

let todoItems = [];

const addItem = () => {
  todoItems.push(inputDiv.value);
  console.log(todoItems);

  outputDiv.innerHTML = "";
  inputDiv.value = "";
  todoItems.forEach((item) => {
    outputDiv.innerHTML += `
        <div>${item}</div>
    `;
  });
};

// // for, while, do while, foreach, for in, map

// for (i = 1; i <= 10; i++) {
//   console.log(`Multiplication table of ${i} is: `);
//   for (j = 1; j <= 10; j++) {
//     console.log(`${i} x ${j}: `, i * j);
//   }
// }

// let count = 0;
// while (count > 5) {
//   console.log("Iteration ", count);
//   count++;
// }

// let secondCount = 0;
// do {
//   console.log("Do Iteration ", secondCount);
//   secondCount++;
// } while (secondCount > 5);

// let names = ["subekshya", "sambhav", "karanjit", "rijal"];

// for (i = 0; i < names.length; i++) {
//   console.log(names[i]);
// }

// for (item in names) {
//   console.log("For in loop", item, names[item]);
// }

// names.forEach((v, key) => {
//   console.log("Name is", v, key);
// });
