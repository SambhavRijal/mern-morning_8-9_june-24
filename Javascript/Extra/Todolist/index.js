let todoItems = [];
const outputDiv = document.querySelector(".output");

let todoItem = {
  title: "",
  status: "incomplete",
};

const addItem = (event) => {
  event.preventDefault();
  todoItems.push(todoItem);
  renderItems();
};

const renderItems = () => {
  outputDiv.innerHTML = "";
  todoItems.forEach((item) => {
    // if (item.status == "incomplete") {
    //   outputDiv.innerHTML += `
    //   <div>${item.title}  ${item.status}
    //   <input type="checkbox" />
    //   </div>
    //   `;
    // } else {
    //   outputDiv.innerHTML += `
    //   <div>${item.title}  ${item.status}
    //    <input type="checkbox" checked />
    //   </div>
    //   `;
    // }

    outputDiv.innerHTML += `
    <div>${item.title} <span class="${
      item.status == "complete" ? "green" : "red"
    }"> ${item.status}<span>
    <input type="checkbox" ${item.status == "complete" && "checked"} />
    </div>
`;
  });
};

// let x=5
// x>5 ? 'X is greather than 5' : 'x is less than 5'

// x>5 && 'X is greather than 5'

const handleChange = (event) => {
  todoItem = {
    ...todoItem,
    [event.target.name]: event.target.value,
  };
};

const clearForm = () => {
  todoItem = {
    title: "",
    status: "incomplete",
  };

  document.querySelector('input[name:"title"]').value = "";
  document.querySelector('input[name:"status"]').value = "incomplete";
};
