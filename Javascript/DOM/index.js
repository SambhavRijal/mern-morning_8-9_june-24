// document.getElementById("content").innerHTML = "Changed stuff";

// console.log(contentDiv);

let content = "Helloo world";

let contentDiv = document.querySelector("#content");
let dialogContainer = document.querySelector(".dialog");

document.querySelector(".dialog").addEventListener("click", closeDialog);

function closeDialog() {
  dialogContainer.style.display = "none";
}

function changeDivContent() {
  //   contentDiv.innerHTML = contentDiv.innerHTML + content;
  contentDiv.innerHTML += content;

  //   a=a+b
  //   a+=b
}

function changeContent() {
  //   contentDiv.style.color = "green";
  contentDiv.classList.add("animate-background");
}

function makeBold() {
  contentDiv.classList.add("bold");
}

function showDialog() {
  dialogContainer.style.display = "flex";
}
