// let x = 10;

// let y = 101;

// let total = x + y;
// console.log("Sum is", total);

// if (total >= 20) {
//   console.log("Sum is greater than 20");
// } else if (total == 20) {
//   console.log("Sum is equal to 20");
// } else {
//   console.log("sum is less than 20");
// }

let input = document.getElementById("number");
let output = document.querySelector(".output");

function checkEven() {
  if (input.value % 2 == 0) {
    output.innerHTML = `The entered number ${input.value} is even`;
  } else {
    output.innerHTML = `The entered number ${input.value} is odd`;
  }
}

//   inputValue%2 --> will give remainder either 0 or 1

// +,-,/,*

// >,<,==,>=,<=

// Not !

// = -> value assign

// == -> compare (loose comparision)

// === -> strict (strict comparision based on type)

// And OR
