const express = require("express");
const router = express.Router();

const controller = require("./controller");

router.get("", controller.get);

router.post("", controller.create);

router.get("/category/:category", controller.getByCategory);

router.get("/user/:user", controller.getByUser);

router.put("/:id", controller.update);

router.delete("/:id", controller.deleteOne);

module.exports = router;
