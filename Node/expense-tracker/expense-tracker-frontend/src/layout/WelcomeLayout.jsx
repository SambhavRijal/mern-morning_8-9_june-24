import React from "react";
import { Route, Routes } from "react-router";
import HomePage from "../pages/main/home";

const WelcomeLayout = () => {
  return (
    <Routes>
      <Route path="" element={<HomePage />} />
      <Route path="/about" element={<div>About page</div>} />
      <Route path="/*" element={<div>404 not found</div>} />
    </Routes>
  );
};

export default WelcomeLayout;
