import React from "react";
import { Navigate, Route, Routes } from "react-router";
import DashboardHome from "../pages/dashboard/home";
import DashboardTransactions from "../pages/dashboard/transactions";
import DashboardHeader from "../components/partials/DashboardHeader";
import DashboardSidebar from "../components/partials/DashboardSidebar";
import DashboardAccounts from "../pages/dashboard/accounts";
import DashboardCategories from "../pages/dashboard/categories";

const DashboardLayout = () => {
  return (
    <div>
      <DashboardHeader />

      <div className="flex h-screen">
        <div className="left w-[150px]">
          <DashboardSidebar />
        </div>
        <div className="right grow pl-sm pt-sm mr-sm">
          <Routes>
            <Route path="/" element={<Navigate to="/dashboard/summary" />} />
            <Route path="/summary" element={<DashboardHome />} />
            <Route path="/accounts" element={<DashboardAccounts />} />
            <Route path="/categories" element={<DashboardCategories />} />

            <Route path="/transactions" element={<DashboardTransactions />} />
            <Route path="/*" element={<div>404 not found</div>} />
          </Routes>
        </div>
      </div>
    </div>
  );
};

export default DashboardLayout;
