import React from "react";
import { Navigate, Route, Routes } from "react-router";
import Login from "../pages/authentication/Login";
import SignupScreen from "../pages/authentication/SignupScreen";

const AuthLayout = () => {
  return (
    <Routes>
      <Route path="/" element={<Navigate to="login" />} />
      <Route path="/login" element={<Login />} />
      <Route path="/signup" element={<SignupScreen />} />
      <Route path="/*" element={<div>404 not found</div>} />
    </Routes>
  );
};

export default AuthLayout;
