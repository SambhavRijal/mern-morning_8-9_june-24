import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { BASE_URL } from "../../utils/BaseURL";
import axios from "axios";

const Login = () => {
  const [loginData, setLoginData] = useState({
    email: "",
    password: "",
  });

  const navigate = useNavigate();
  const handleChange = (event) => {
    setLoginData({
      ...loginData,
      [event.target.name]: event.target.value,
    });
  };

  const submitForm = async (event) => {
    event.preventDefault();
    console.log(loginData);
    const res = await axios.post(`${BASE_URL}/auth/login`, loginData);
    console.log(res.data.data);

    localStorage.setItem("token", res.data.data);
    
  };

  return (
    <div className="h-screen w-screen flex justify-center items-center">
      <form
        action=""
        className="w-1/2 h-1/2 shadow-md shadow-orange-700 flex flex-col gap-md justify-center items-center rounded-md"
        onSubmit={submitForm}
      >
        <div className="flex flex-col w-2/3">
          <label htmlFor="">Email</label>
          <input
            onChange={handleChange}
            value={loginData.email}
            type="email"
            name="email"
            className="border-2 rounded-md"
          />
        </div>
        <div className="flex flex-col w-2/3">
          <label htmlFor="">Password</label>
          <input
            onChange={handleChange}
            value={loginData.password}
            type="password"
            name="password"
            className="border-2 rounded-md"
          />
        </div>
        <div className="w-2/3">
          <button
            className="bg-orange-700 hover:bg-orange-500 text-white font-bold text-lg w-full"
            type="submit"
          >
            Submit
          </button>
        </div>
        <div>
          Dont have an account?{" "}
          <Link to="/auth/signup" className="text-blue-500">
            Signup
          </Link>
        </div>
      </form>
    </div>
  );
};

export default Login;
