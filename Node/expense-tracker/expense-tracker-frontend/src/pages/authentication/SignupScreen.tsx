import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import { BASE_URL } from "../../utils/BaseURL";

const SignupScreen = () => {
  const [signupData, setSignupData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    phoneNumber: "",
    password: "",
    confirmPassword: "",
  });

  const navigate = useNavigate();

  const handleChange = (event) => {
    setSignupData({
      ...signupData,
      [event.target.name]: event.target.value,
    });
  };

  const submitForm = async (event) => {
    event.preventDefault();

    const { confirmPassword, ...remainingValues } = signupData;

    if (signupData.password == confirmPassword) {
      console.log(remainingValues);
      console.log(BASE_URL);
      const res = await axios.post(`${BASE_URL}/users`, remainingValues);
      if (res.data.status == 201) {
        navigate("/auth/login");
      }
    } else {
      console.log("Passwords must match");
    }
  };

  return (
    <div className="h-screen w-screen flex justify-center items-center">
      <form
        action=""
        className="w-1/2 py-xl shadow-md shadow-orange-700 flex flex-col gap-md justify-center items-center rounded-md"
        onSubmit={submitForm}
      >
        <div className="flex flex-col w-2/3">
          <label htmlFor="">FirstName</label>
          <input
            onChange={handleChange}
            value={signupData.firstName}
            type="text"
            name="firstName"
            className="border-2 rounded-md"
          />
        </div>
        <div className="flex flex-col w-2/3">
          <label htmlFor="">LastName</label>
          <input
            onChange={handleChange}
            value={signupData.lastName}
            type="text"
            name="lastName"
            className="border-2 rounded-md"
          />
        </div>
        <div className="flex flex-col w-2/3">
          <label htmlFor="">Phone Number</label>
          <input
            onChange={handleChange}
            value={signupData.phoneNumber}
            type="number"
            name="phoneNumber"
            className="border-2 rounded-md"
          />
        </div>
        <div className="flex flex-col w-2/3">
          <label htmlFor="">Email</label>
          <input
            onChange={handleChange}
            value={signupData.email}
            type="email"
            name="email"
            className="border-2 rounded-md"
          />
        </div>
        <div className="flex flex-col w-2/3">
          <label htmlFor="">Password</label>
          <input
            onChange={handleChange}
            value={signupData.password}
            type="password"
            name="password"
            className="border-2 rounded-md"
          />
        </div>
        <div className="flex flex-col w-2/3">
          <label htmlFor="">Confirm Password</label>
          <input
            onChange={handleChange}
            value={signupData.confirmPassword}
            type="password"
            name="confirmPassword"
            className="border-2 rounded-md"
          />
        </div>
        <div className="w-2/3">
          <button
            className="bg-orange-700 hover:bg-orange-500 text-white font-bold text-lg w-full"
            type="submit"
          >
            Submit
          </button>
        </div>
        <div>
          Already have an account?{" "}
          <Link to="/auth/login" className="text-blue-500">
            Login
          </Link>
        </div>
      </form>
    </div>
  );
};

export default SignupScreen;
