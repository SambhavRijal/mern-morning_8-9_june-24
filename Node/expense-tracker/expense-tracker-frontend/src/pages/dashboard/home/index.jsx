import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  increaseCounter,
  setCounter,
} from "../../../store/modules/count/actions";

const DashboardHome = () => {
  const dispatch = useDispatch();

  const increaseCount = () => {
    dispatch(setCounter(50));
  };
  const count = useSelector((state) => state.countReducer.count);

  return (
    <div>
      Dashboard Home Page {count}
      <button onClick={increaseCount}>click</button>
    </div>
  );
};

export default DashboardHome;
