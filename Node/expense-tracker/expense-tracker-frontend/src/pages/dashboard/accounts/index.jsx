import React, { useEffect, useState } from "react";
import axios from "axios";

const DashboardAccounts = () => {
  const [account, setAccount] = useState({
    title: "",
    amount: 0,
    user: "66bd79b6b67c2086483e089d",
  });

  const [accounts, setAccounts] = useState([]);

  useEffect(() => {
    getAccounts();
  }, []);

  const handleChange = (event) => {
    setAccount({
      ...account,
      [event.target.name]: event.target.value,
    });
  };

  const getAccounts = async () => {
    const res = await axios.get("http://localhost:3002/accounts");
    setAccounts(res.data.data);
    console.log(res.data.data);
  };

  const createAccount = async (event) => {
    event.preventDefault();
    console.log(account);
    const res = await axios.post("http://localhost:3002/accounts", account);

    getAccounts();

    console.log(res.data.data);
  };

  return (
    <div className="">
      <form
        action="
    "
        onSubmit={createAccount}
        className="flex flex-col gap-sm w-1/2 mx-auto my-sm"
      >
        <div className="flex gap-sm">
          <label htmlFor="" className="w-1/12">
            Title
          </label>
          <input
            type="text"
            name="title"
            className="border-2 grow"
            value={account.title}
            onChange={handleChange}
          />
        </div>{" "}
        <div className="flex gap-sm">
          <label htmlFor="" className="w-1/12">
            Amount
          </label>
          <input
            type="number"
            name="amount"
            className="border-2 grow"
            value={account.amount}
            onChange={handleChange}
          />
        </div>
        <button
          type="submit"
          className="border-2 bg-blue-500 hover:bg-blue-800 text-white"
        >
          Submit
        </button>
      </form>

      <div className="transactions border-2 p-xl">
        {/* <button
          type="submit"
          onClick={getAccounts}
          className="border-2 bg-green-500 hover:bg-green-800 text-white py-xs my-lg w-full"
        >
          Get Accounts
        </button> */}

        <div>
          {accounts.map((v, key) => (
            <div key={key} className="mb-sm border-2">
              <p className="font-semibold">{v.title}</p>
              <p>{v.amount}</p>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default DashboardAccounts;
