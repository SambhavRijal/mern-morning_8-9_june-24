import React, { useEffect, useState } from "react";
import axios from "axios";
import { GetRequest, PostRequest } from "../../../plugins/https";
import { useSelector } from "react-redux";

const DashboardTransactions = () => {
  const [transaction, setTransaction] = useState({
    amount: 0,
    category: "",
    account: "",
    title: "",
  });

  const [transactions, setTransactions] = useState([]);

  const [categories, setCategories] = useState([]);
  const [accounts, setAccounts] = useState([]);

  const handleChange = (event) => {
    setTransaction({
      ...transaction,
      [event.target.name]: event.target.value,
    });
  };

  useEffect(() => {
    getTransactions();
    getFormSelectData();
  }, []);

  const getTransactions = async () => {
    // const res = await axios.get("http://localhost:3002/transactions");
    const res = await GetRequest("/transactions");
    setTransactions(res.data);
  };

  const createTransaction = async (event) => {
    event.preventDefault();
    if (
      transaction.category.length > 0 &&
      transaction.account.length > 0 &&
      transaction.title.length > 0
    ) {
      // const res = await axios.post(
      //   "http://localhost:3002/transactions",
      //   transaction
      // );

      const res = await PostRequest("/transactions", transaction);
      getTransactions();
    }
  };

  const getFormSelectData = async () => {
    const categoryRes = await axios.get("http://localhost:3002/categories");
    setCategories(categoryRes.data.data);

    const accountRes = await axios.get("http://localhost:3002/accounts");
    setAccounts(accountRes.data.data);

    // setTransaction({
    //   ...transaction,
    //   category: categoryRes.data.data[0]._id,
    //   account: accountRes.data.data[0]._id,
    // });
  };

  const count = useSelector((state) => state.countReducer.count);

  return (
    <div>
      {count}
      <form
        action="
    "
        onSubmit={createTransaction}
        className="flex flex-col gap-sm"
      >
        <div className="flex gap-sm">
          <label htmlFor="" className="w-1/12">
            Amount
          </label>
          <input
            type="number"
            name="amount"
            className="border-2 grow"
            value={transaction.amount}
            onChange={handleChange}
          />
        </div>
        <div className="flex gap-sm">
          <label htmlFor="" className="w-1/12">
            Category
          </label>
          {/* <input
            type="text"
            name="category"
            className="border-2 grow"
            value={transaction.category}
            onChange={handleChange}
          /> */}
          <select
            name="category"
            id=""
            className="border-2 grow"
            value={transaction.category}
            onChange={handleChange}
          >
            <option value="" selected={true}>
              -- Category --
            </option>
            {categories.map((v, key) => (
              <option value={v._id} key={key}>
                {v.title}
              </option>
            ))}
          </select>
        </div>{" "}
        <div className="flex gap-sm">
          <label htmlFor="" className="w-1/12">
            Account
          </label>
          {/* <input
            type="text"
            name="account"
            className="border-2 grow"
            value={transaction.account}
            onChange={handleChange}
          /> */}
          <select
            name="account"
            id=""
            className="border-2 grow"
            value={transaction.account}
            onChange={handleChange}
          >
            <option value="" selected={true}>
              -- Account --
            </option>
            {accounts.map((v, key) => (
              <option value={v._id} key={key}>
                {v.title}
              </option>
            ))}
          </select>
        </div>
        <div className="flex gap-sm">
          <label htmlFor="" className="w-1/12">
            Remarks
          </label>
          <textarea
            name="title"
            id=""
            className="border-2 grow"
            value={transaction.title}
            onChange={handleChange}
          ></textarea>
        </div>
        <button
          type="submit"
          className="border-2 bg-blue-500 hover:bg-blue-800 text-white"
        >
          Submit
        </button>
      </form>

      <div className="transactions border-2 p-xl">
        {/* <button
          type="submit"
          onClick={getTransactions}
          className="border-2 bg-green-500 hover:bg-green-800 text-white py-xs my-lg w-full"
        >
          Get Transactions
        </button> */}

        <div>
          {transactions.map((v, key) => (
            <div key={key} className="mb-sm border-2">
              <p className="font-semibold">Amount: Rs {v.amount}</p>
              <p>Title: {v.title}</p>
              <p>Account: {v.account.title}</p>
              <p>Category: {v.category.title}</p>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default DashboardTransactions;
