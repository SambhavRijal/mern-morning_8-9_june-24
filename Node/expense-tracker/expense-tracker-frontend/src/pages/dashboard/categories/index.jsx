import React, { useEffect, useState } from "react";
import axios from "axios";
import DashboardTransactions from "../transactions";

const DashboardCategories = () => {
  const [category, setCategory] = useState({
    title: "",
    user: "66bd79b6b67c2086483e089d",
  });

  const [categories, setCategories] = useState([]);

  const handleChange = (event) => {
    setCategory({
      ...category,
      [event.target.name]: event.target.value,
    });
  };

  useEffect(() => {
    getCategories();
  }, []);

  const getCategories = async () => {
    const res = await axios.get("http://localhost:3002/categories");
    setCategories(res.data.data);
    console.log(res.data.data);
  };

  const createCategory = async (event) => {
    event.preventDefault();
    console.log(category);
    const res = await axios.post("http://localhost:3002/categories", category);
    getCategories();
    resetForm();

    console.log(res.data.data);
  };

  const resetForm = () => {
    setCategory({
      title: "",
      user: "66bd79b6b67c2086483e089d",
    });
  };

  return (
    <div className="">
      <form
        action="
    "
        onSubmit={createCategory}
        className="flex flex-col gap-sm w-1/2 mx-auto my-sm"
      >
        <div className="flex gap-sm">
          <label htmlFor="" className="w-1/12">
            Title
          </label>
          <input
            type="text"
            name="title"
            className="border-2 grow"
            value={category.title}
            onChange={handleChange}
          />
        </div>{" "}
        <button
          type="submit"
          className="border-2 bg-blue-500 hover:bg-blue-800 text-white"
        >
          Submit
        </button>
      </form>

      <div className="transactions border-2 p-xl">
        {/* <button
          type="submit"
          onClick={getCategories}
          className="border-2 bg-green-500 hover:bg-green-800 text-white py-xs my-lg w-full"
        >
          Get Categories
        </button> */}

        <div>
          {categories?.map((v, key) => (
            <div key={key} className="mb-sm border-2">
              <p className="font-semibold">{v.title}</p>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default DashboardCategories;
