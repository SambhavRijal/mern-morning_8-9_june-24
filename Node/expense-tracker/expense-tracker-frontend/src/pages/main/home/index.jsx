import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { increaseCounter } from "../../../store/modules/count/actions";

const HomePage = () => {
  const count = useSelector((state) => state.countReducer.count);

  return (
    <div>
      HomePage
      {count}
      <button onClick={increaseCount}>Increase</button>
    </div>
  );
};

export default HomePage;
