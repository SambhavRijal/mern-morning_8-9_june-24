import { combineReducers } from "redux";
import { countReducer } from "./modules/count/reducer";

export default combineReducers({
  countReducer,
});
