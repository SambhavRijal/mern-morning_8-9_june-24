import { Navigate, Route, Routes, useNavigate } from "react-router";
import "./App.css";
import WelcomeLayout from "./layout/WelcomeLayout";
import DashboardLayout from "./layout/DashboardLayout";
import AuthLayout from "./layout/AuthLayout";

function App() {
  const token = localStorage.getItem("token");

  const navigate = useNavigate();

  const handleLogout = () => {
    localStorage.clear();
    window.location.reload();
    navigate("/");
  };
  return (
    <div>
      {token ? (
        <button onClick={handleLogout}>Logout</button>
      ) : (
        <button
          oncClick={() => {
            navigate("/auth/login");
          }}
        >
          Login
        </button>
      )}
      <Routes>
        <Route
          path="/dashboard/*"
          element={token ? <DashboardLayout /> : <Navigate to="/" />}
        />
        <Route
          path="/auth/*"
          element={token ? <Navigate to="/dashboard" /> : <AuthLayout />}
        />
        <Route path="/*" element={<WelcomeLayout />} />
      </Routes>
    </div>
  );
}

export default App;
