import React from "react";
import { NavLink } from "react-router-dom";

const DashboardSidebar = () => {
  return (
    <div className="border-r-2 h-full  ">
      <ul className="list-none flex flex-col sidenav ">
        <NavLink
          to={"/dashboard/summary"}
          className="px-md  border-b-2 py-sm hover:bg-gray-300 cursor-pointer"
        >
          Dashboard
        </NavLink>
        <NavLink
          to={"/dashboard/transactions"}
          className="px-md  border-b-2 py-sm hover:bg-gray-300 cursor-pointer"
        >
          Transactions
        </NavLink>
        <NavLink
          to={"/dashboard/categories"}
          className="px-md  border-b-2 py-sm hover:bg-gray-300 cursor-pointer"
        >
          Categories
        </NavLink>
        <NavLink
          to={"/dashboard/accounts"}
          className="px-md  border-b-2 py-sm hover:bg-gray-300 cursor-pointer"
        >
          Accounts
        </NavLink>
        {/* <li className="px-md  border-b-2 py-sm hover:bg-gray-300 cursor-pointer">
          Transactions
        </li> */}
      </ul>
    </div>
  );
};

export default DashboardSidebar;
