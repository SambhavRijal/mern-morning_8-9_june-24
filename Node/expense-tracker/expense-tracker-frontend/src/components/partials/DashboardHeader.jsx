import React from "react";

const DashboardHeader = () => {
  return (
    <div className="bg-blue-600 text-white py-sm px-sm font-bold text-xl">
      DashboardHeader
    </div>
  );
};

export default DashboardHeader;
