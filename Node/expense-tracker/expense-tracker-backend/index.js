const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");

const transactionRouter = require("./modules/transaction/router");
const categoryRouter = require("./modules/category/router");
const accountRouter = require("./modules/account/router");
const userRouter = require("./modules/user/router");
const authRouter = require("./modules/authentication/router");
// Create, Read, Update, Delete
// Post , Get , Put, Patch, delete

const PORT = 3002;

// id,account, amount greather than 20000, less than 20000, by type
// Make api to create new transaction

// Make new array of accounts, category, users, make apis (get and post) for them
// When making get api in life, always make a getOne, in apis for account i need apis to get greather than
//  and letss than specified amount

const app = express();
app.use(express.json());
app.use(cors());

app.get("", (req, res) => {
  res.send("Home page is called");
});

app.use("/transactions", transactionRouter);
app.use("/categories", categoryRouter);
app.use("/accounts", accountRouter);
app.use("/users", userRouter);
app.use("/auth", authRouter);

app.listen(PORT, () => {
  console.log("Sever is running on port ", PORT);

  try {
    mongoose.connect("mongodb://localhost:27017/expensetracker");
    console.log("Database connected successfully");
  } catch (error) {
    console.log("Connection failed with datbase", error);
  }
});

//  title, account, category, amount (form Handling, submit->console.log())
//  transactions -> transaction form,   accounts-> account form, catgory-> cateogry form, signup-> form (form handling)
