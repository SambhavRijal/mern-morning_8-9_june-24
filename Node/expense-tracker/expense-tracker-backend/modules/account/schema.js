const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const accountSchema = new Schema({
  title: {
    type: "String",
    required: true,
  },
  image: {
    type: "String",
    default: "",
  },
  amount: {
    type: "Number",
    default: 0,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
});

module.exports = mongoose.model("Account", accountSchema);
