const Schema = require("./schema");

const get = async (req, res) => {
  const data = await Schema.find({
    user: req.user._id,
  }).populate("category account");
  res.send({
    status: 200,
    message: "Data successfully retrieved",
    data: data,
  });
};

const create = async (req, res) => {
  try {
    const data = await Schema.create({
      ...req.body,
      user: req.user._id,
    });
    res.send({
      status: 201,
      message: "Data inserted successfully",
      data: data,
    });
  } catch (error) {
    console.log("Error occured", error);
    res.send({
      status: 201,
      message: "Data Insertion Failed",
    });
  }
};

const update = async (req, res) => {
  try {
    const data = await Schema.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });

    res.send({
      status: 201,
      message: "Sucessfully updated",
      data: data,
    });
  } catch (error) {
    console.log("Error encountered ", error);
    res.send({
      status: 500,
      message: "Failed to execute the task",
      data: error.message,
    });
  }
};

const getByCategory = async (req, res) => {
  console.log(req.params.category);
  const data = await Schema.find({
    category: req.params.category,
  });

  res.send({
    status: 200,
    message: "Data successfully retrieved",
    data: data,
  });
};

const getByUser = async (req, res) => {
  console.log(req.params.user);
  const data = await Schema.find({
    user: req.params.user,
  }).populate("user", "firstName lastName");

  res.send({
    status: 200,
    message: "Data successfully retrieved",
    data: data,
  });
};

const deleteOne = async (req, res) => {
  const data = await Schema.findByIdAndDelete(req.params.id);
  res.send({
    status: 201,
    message: "Successfully deleted item",
  });
};

module.exports = {
  get,
  create,
  getByCategory,
  getByUser,
  update,
  deleteOne,
};
