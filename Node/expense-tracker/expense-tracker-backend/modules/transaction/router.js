const express = require("express");
const router = express.Router();
const { verifyUser } = require("../authentication/auth.middleware");

const controller = require("./controller");

router.get("", verifyUser, controller.get);

router.post("", verifyUser, controller.create);

router.get("/category/:category", controller.getByCategory);

router.get("/user/:user", controller.getByUser);

router.put("/:id", controller.update);

router.delete("/:id", controller.deleteOne);

module.exports = router;
