const Schema = require("./schema");

const bcrypt = require("bcrypt");

const get = async (req, res) => {
  const data = await Schema.find();
  res.send({
    status: 200,
    message: "Data successfully retrieved",
    data: data,
  });
};

const create = async (req, res) => {
  try {
    const userDetails = { ...req.body };
    const hashedPassword = await bcrypt.hash(userDetails.password, 10);
    userDetails.password = hashedPassword;
    let data = await Schema.create(userDetails);
    data = data.toObject();
    delete data.password;
    res.send({
      status: 201,
      message: "Data inserted successfully",
      data: data,
    });
  } catch (error) {
    console.log("Error occured", error);
  }
};

const update = async (req, res) => {
  try {
    const data = await Schema.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });

    res.send({
      status: 201,
      message: "Sucessfully updated",
      data: data,
    });
  } catch (error) {
    console.log("Error encountered ", error);
    res.send({
      status: 500,
      message: "Failed to execute the task",
      data: error.message,
    });
  }
};

const deleteOne = async (req, res) => {
  const data = await Schema.findByIdAndDelete(req.params.id);
  res.send({
    status: 201,
    message: "Successfully deleted item",
  });
};

module.exports = {
  get,
  create,
  update,
  deleteOne,
};
