const Schema = require("../user/schema");
const bcrypt = require("bcrypt");

const jwt = require("jsonwebtoken");

const login = async (req, res) => {
  const existingUser = await Schema.findOne({
    email: req.body.email,
  });

  if (existingUser) {
    const isPasswordCorrect = await bcrypt.compare(
      req.body.password,
      existingUser.password
    );

    if (isPasswordCorrect) {
      const token = await jwt.sign(
        {
          id: existingUser._id,
          email: existingUser.email,
        },
        "EXPENSE_TRACKER",
        {
          expiresIn: "30d",
        }
      );

      res.send({
        status: 200,
        message: "Login successful",
        data: token,
      });
    } else {
      res.send({
        status: 200,
        message: "Invalid Credentials",
      });
    }
  } else {
    res.send({
      status: 500,
      message: "User doesnt exist with the specified email",
    });
  }
};

module.exports = {
  login,
};
