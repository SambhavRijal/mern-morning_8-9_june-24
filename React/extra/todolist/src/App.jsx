import logo from "./logo.svg";
import "./App.css";
import { useState } from "react";

function App() {
  // let name = "sambhav";

  const [newName, setNewName] = useState("Sambhav");
  const [names, setNames] = useState(["sambhav", "subu", "shristi"]);

  // console.log(name);

  const [name, setName] = useState("");

  const changeName = () => {
    // name = "sth else";

    setNewName("subu");

    // document.querySelector(".output").innerHTML = name;
  };

  const addNameToList = (event) => {
    event.preventDefault();
    setNames([...names, name]);

    setName("");
  };

  const handleInput = (event) => {
    setName(event.target.value);
  };

  const deleteItem = (index) => {
    // const filteredItems = names.filter((v, key) => key !== index);
    // console.log(filteredItems);
    // setNames(filteredItems);
    setNames(names.filter((v, key) => key !== index));
  };

  return (
    <div className="App">
      Hello World
      {/* <div className="output">{name}</div>
      <div className="">{newName}</div>
      <button onClick={changeName}>Click to change name</button> */}
      <h1>Working with arrays</h1>
      <form action="" onSubmit={addNameToList}>
        <input type="text" onChange={handleInput} value={name} />
        <button>Click to add name</button>
      </form>
      <div>
        {names.map((v, key) => (
          <div>
            {key + 1}.{v}
            <button
              style={{
                margin: "10px 10px",
              }}
              onClick={() => {
                deleteItem(key);
              }}
            >
              Delete
            </button>
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;
