import { useState } from "react";

export const TodoPage = () => {
  const [name, setName] = useState("sambhav");
  const [todoItems, setTodoItems] = useState([]);

  const [selectedIndex, setSelectedIndex] = useState(-1);

  const handleChange = (event) => {
    setTodoItem({
      ...todoItem,
      [event.target.name]: event.target.value,
    });
  };

  const [todoItem, setTodoItem] = useState({
    title: "",
    status: "incomplete",
  });

  const submit = (event) => {
    event.preventDefault();
    if (selectedIndex == -1) {
      setTodoItems([...todoItems, todoItem]);
    } else {
      let modifiedItems = [...todoItems];
      modifiedItems[selectedIndex] = todoItem;
      setTodoItems(modifiedItems);
      setSelectedIndex(-1);
    }

    setTodoItem({
      title: "",
      status: "incomplete",
    });
  };

  const deleteItem = (index) => {
    setTodoItems(todoItems.filter((item, i) => i != index));
  };

  const handleCheck = (index) => {
    setTodoItems(
      todoItems.map((item, i) => {
        if (i === index) {
          item.status = item.status == "incomplete" ? "complete" : "incomplete";
        }
        return item;
      })
    );
  };

  return (
    <div>
      <form
        action=""
        onSubmit={submit}
        className="flex gap-7 bg-success-default"
      >
        <div>
          <input
            className="border-2 "
            type="text"
            onChange={handleChange}
            name="title"
            value={todoItem.title}
          />
        </div>
        <select
          name="status"
          id=""
          onChange={handleChange}
          className="border-2 "
        >
          <option value="incomplete">Incomplete</option>
          <option value="complete">Complete</option>
        </select>

        <button
          type="submit"
          className="border-2 text-red-700 hover:text-blue-400   "
        >
          Add
        </button>
      </form>

      <div
        style={{
          width: "50%",
        }}
      >
        {todoItems.map((item, index) => (
          <div
            key={index}
            className={`${item.status == "complete" && "linethrough"}`}
            style={{
              display: "flex",
              justifyContent: "space-between",
              gap: "10px",
            }}
          >
            {item.title}
            <button
              onClick={() => {
                deleteItem(index);
              }}
            >
              Delete
            </button>
            <button
              onClick={() => {
                setSelectedIndex(index);
                console.log(selectedIndex);
                setTodoItem(todoItems[index]);
              }}
            >
              Edit
            </button>
            <input
              type="checkbox"
              checked={item.status == "incomplete" ? false : true}
              onChange={() => {
                handleCheck(index);
              }}
            />
          </div>
        ))}
      </div>

      <div className="flex flex-col md:flex-row">
        <div className=" bg-red-400 h-[200px] md:w-1/2">eee</div>
        <div className=" bg-green-400 h-[200px] md:w-1/2">eee</div>
      </div>
    </div>
  );
};
