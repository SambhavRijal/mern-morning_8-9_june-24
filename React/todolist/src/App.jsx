import { useState } from "react";
import "./App.css";
import { TodoPage } from "./pages/home";
import { AboutPage } from "./pages/about";
import AppRouter from "./AppRouter";
import { Link, NavLink } from "react-router-dom";

function App() {
  return (
    <div>
      <div>
        <span>
          <NavLink to={"/about"}>About</NavLink>{" "}
        </span>{" "}
        <span>
          <NavLink to={"/"}>Home</NavLink>{" "}
        </span>
        <span>
          <NavLink to={"/contactus"}>Contact</NavLink>{" "}
        </span>
      </div>
      <AppRouter />
    </div>
  );
}

export default App;
