import React from "react";
import { Route, Routes } from "react-router";
import { AboutPage } from "./pages/about";
import { TodoPage } from "./pages/home";
import { ContactUsPage } from "./pages/contact";

const AppRouter = () => {
  return (
    <div>
      <Routes>
        <Route path={"/about"} element={<AboutPage />} />
        <Route path={"/"} element={<TodoPage />} />
        <Route path={"/contactus"} element={<ContactUsPage />} />
      </Routes>
    </div>
  );
};

export default AppRouter;
