import React from "react";
import HeroLeftSection from "./HeroLeftSection";
import HeroImage from "../assets/images/chocolate-8437801_960_720.jpg";

const HomeHero = () => {
  return (
    <div className="hero flex">
      <div className="w-half">
        <HeroLeftSection />
      </div>
      <div className="w-half">
        <img src={HeroImage} alt="" srcset="" />
      </div>
    </div>
  );
};

export default HomeHero;
